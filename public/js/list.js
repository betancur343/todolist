//detecting onclick events
$(function () {

    $("#todo-list").on('click', '.todo-item-delete', function(e) {
        let item = this;
        deleteTodo(e, item)
    })

    //fadding out the info alert
    $('#info-alert').fadeOut(8000, function() {
        $('#info-alert').remove();
    });
    //fadding out the info alert
    $('#success-alert').fadeOut(3000, function() {
        $('#success-alert').remove();
    });
    //fadding out the info alert
    $('#error-alert').fadeOut(3000, function() {
        $('#error-alert').remove();
    });

});

//delete an item
function deleteTodo(e, item) {
    $(item).parent().fadeOut('slow', function() {
        $(item).parent().remove();
    });
}

/**/

