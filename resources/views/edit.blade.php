@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            {{--Edit panel--}}
            <div class="card-header">
                <h5 class="modal-title" id="exampleModalLabel">Editing Item</h5>
            </div>
            <div class="card-body">
                <form id="form-edit-todo" method="post" action="{{route('items.update')}}">
                @csrf
                    <!-- Name input -->
                    <div class="col-md-8">
                        <label for="edit-todo-name">Name:</label>
                        <input type="text" class="form-control" id="edit-todo-name"
                               name="name" placeholder="Enter name" autofocus
                               value="{{$itemSelected->name}}" required >
                    </div>
                    <!-- Priority input -->
                    <div class="col-md-8">
                        <label for="edit-todo-priority">Priority:</label>
                        <select class="form-control" id="edit-todo-priority"
                                name="priority" required >
                            <option value="{{$itemSelected->priority}}" selected>{{$itemSelected->priority}}</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <!-- Expires input -->
                    <div class="col-md-8">
                        <label for="edit-todo-expiration-date">Expires:</label>
                        <input class="form-control" id="edit-todo-expiration-date" type="date" name="expires"
                               value="{{$itemSelected->expires}}" required>
                    </div>
                    <br/>
                    <div class="card-footer">
                        <input type="hidden" name="id" value="{{$itemSelected->id}}" >
                        <a class="btn btn-secondary"  href="{{route('items.show')}}">Back to list !</a>
                        <button id="edit-item" class="btn btn-primary" type="submit">Save item !</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
