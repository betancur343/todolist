@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            {{--Delete panel--}}
            <div class="card-header">
                <h5 class="modal-title" id="exampleModalLabel">Deleting Item</h5>
            </div>
            <div class="card-body">
                <form id="form-delete-todo" method="post" action="{{route('items.delete2')}}" >
                    @csrf
                    <div class="col-md-8">
                        <input id="delete-name" class="form-control" name="name" value="{{$itemSelected->name}}"
                               readonly>
                        <label for="delete-name">Are you sure ?</label>
                        <input type="hidden" name="id" value="{{$itemSelected->id}}" >
                    </div>
                    <br/>
                    <div class="card-footer">
                        <a class="btn btn-secondary"  href="{{route('items.show')}}">Back to list !</a>
                        <button id="add-item" class="btn btn-primary" type="submit">Delete !</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
