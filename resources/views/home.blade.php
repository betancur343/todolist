@extends('layouts.app')

@section('content')

    <div class="container">

        {{--Alert--}}
        <div class="alert alert-info alert-dismissible fade show" role="alert" id="info-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>Welcome !</p>
            <p>Remember, you've forgotten <strong>{{$expired}}</strong> item(s) and <strong>{{$next2expire}}</strong> expires today.</p>
        </div>
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert" id="success-alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="error-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>{{session('error')}}</p>
        </div>
        @endif
        {{--Button trigger modal--}}
        <div class="text-center">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addItemModal"
                    autofocus>Create a new item here !</button>
        </div>

        {{--Create Modal--}}
        <div class="modal fade" id="addItemModal" tabindex="-1" role="dialog"
             aria-labelledby="addItemModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Adding Item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-add-todo" action="{{route('items.create')}}" method="post" >
                            @csrf
                            {{--Name input--}}
                            <div class="col-md-8">
                                <label for="todo-name">Name:</label>
                                <input type="text" class="form-control" id="todo-name" autofocus
                                       name='name' placeholder="Enter name" required >
                            </div>
                            {{--Priority input--}}
                            <div class="col-md-8">
                                <label for="todo-priority">Priority:</label>
                                <select class="form-control" id="todo-priority"
                                        name='priority' required>
                                    <option selected></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                            {{--Date input--}}
                            <div class="col-md-8">
                                <label for="todo-expiration-date">Expires:</label>
                                <input class="form-control" id="todo-expiration-date" type="date"
                                       name='expires' required>
                            </div>
                            <br/>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Back to list !
                                </button>
                                <button id="add-item" class="btn btn-primary"
                                        type="submit">&plus; Add item !
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{--List panel--}}
        <div id="todo-list">

                    {{--Item List--}}
                    <ul id="todo-list" class="todo-list">
                        @foreach($items as $item)
                            <div class='card item-card col-md-6'>
                                <div class='card-body'>
                                    <h5 class='card-title' id='name-text'> {{$item->name}}</h5>
                                    <h6 class='card-subtitle text text-muted'
                                        id='priority-text'>Priority: {{$item->priority}}</h6>
                                    <p class='card-text' id='expiration-text'>Expires: {{$item->expires}}</p>
                                    <a id="edit-btn" class='todo-item-edit btn btn-primary'
                                       href="{{route('items.edit', $item->id)}}">Edit !</a>
                                    <a id="delete-btn" class='todo-item-delete btn btn-primary'
                                       href="{{route('items.delete', $item->id)}}">Delete !</a>
                                </div>
                            </div>
                        @endforeach
                    </ul>
                </div>


    </div>

@endsection
