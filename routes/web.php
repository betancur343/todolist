<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// getting Welcome view
Route::get('/', function () {
    return view('welcome');
});

// getting Home view
Auth::routes();

Route::post('/home', 'HomeController@index')->name('home');

// creating items
Route::post('/create', 'ItemController@create')->name('items.create');

// getting items
Route::get('/home', 'ItemController@show')->name('items.show');

// updating items
Route::get('/home/edit/{id}', 'ItemController@edit')->name('items.edit');
Route::post('/home/edit', 'ItemController@update')->name('items.update');

// removing item
Route::get('/home/delete/{id}', 'ItemController@delete')->name('items.delete');
Route::post('/home/delete', 'ItemController@delete2')->name('items.delete2');
