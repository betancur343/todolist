<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

class ItemController extends Controller
{
    /**
     * to create items
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'priority' => 'required',
            'expires' => 'required'
        ]);

        $item = new Item;

        $item['name'] = $request['name'];
        $item['priority'] = $request['priority'];
        $item['expires'] = $request['expires'];
        $item['user_id'] = Auth::id();

        $item->save();

        return redirect()->back()->with('success', 'Item created !');
    }

    /**
     * to show items by user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(){

        //finding only user's items
        $items = User::find(Auth::id())->items;
//        $items = Item::all()->sortByDesc('priority');

        //alerting
        $expired = 0;
        $next2expire = 0;
        foreach ($items as $item){
            if ($item->expires < date('Y-m-d')){
                $expired++;
            }
            if ($item->expires == date('Y-m-d')){
                $next2expire++;
            }
        }
//        echo $expired.$next2expire;

        return view('home', ['items' => $items,
                                    'expired' => $expired,
                                     'next2expire' => $next2expire]);
    }

    /**
     * to edit items
     * @param Integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id){

        $itemSelected = Item::find($id);

        if (Auth::id() != $itemSelected->user_id) {
            return redirect('home')->with('error', 'Unauthorized !');
        }

        return view('edit', ['itemSelected' => $itemSelected]);
    }

    /**
     * to update items
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'priority' => 'required',
            'expires' => 'required'
        ]);

        $item = Item::find($request['id']);

        $item->name = $request['name'];
        $item->priority = $request['priority'];
        $item->expires = $request['expires'];

        $item->save();

        return redirect('home')->with('success', 'Item updated!');
    }

    /**
     * to delete items
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id){

        $itemSelected = Item::find($id);

        if (Auth::id() != $itemSelected->user_id) {
            return redirect('home')->with('error', 'Unauthorized !');
        }
        return view('delete', ['itemSelected' => $itemSelected]);
    }

    /**
     * to delete items
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete2(Request $request){

        if (Item::destroy($request['id']) == 0) {
            return redirect('home')->with('error', 'Something is wrong !');
        } else {
            return redirect('home')->with('success', 'Item deleted !');
        }

    }
}
