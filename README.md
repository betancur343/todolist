<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Deploying TodoList App

You should start by installing:
- *Laravel 5.7*
- *PHP 7.1.9*
- *MySQL 5.7.19*

Then,
1. Clone the repository *https://betancur343@bitbucket.org/betancur343/todolist.git*
2. Download composer.json dependencies.
3. Create a schema and add it to *'.env'* file with the username
 and password of the MySQL server.
4. Run all of the outstanding migrations to create tables and models. Execute: **php artisan
 migrate**
5. Run server and deploy the app executing: **php artisan serve**

#### Notes
If you will want to test the forgot password functionality
configure *'.env'* file with the MAIL_USERNAME and MAIL_PASSWORD 
of your MAIL_DRIVER.
 
---